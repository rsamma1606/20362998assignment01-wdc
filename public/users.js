let btnGet = document.querySelector('button');
let myTable = document.querySelector('#table');

let users = [
	{ uid: 001, 
	email: 'john@dev.com',
	personalInfo: {
		name: 'John',
		address: {
			line1: 'westwhich st',
			line2: 'washmasher',
			city: 'wallas',
			state: 'WX'
			}
		}
	},
	{ uid: 063, 
	email: 'a.abken@latrobe.edu.com',
	personalInfo: {
		name: 'amin',
		address: {
			line1: 'Heildelberg',
			line2: '',
			city: 'Melbourne',
			state: 'VIC'
			}
		}
	},
	{ uid: 045, 
	email: 'Linda.Paterson@gmail.com',
	personalInfo: {
		name: 'Linda',
		address: {
			line1: 'Cherry st',
			line2: 'Kangaroo Point',
			city: 'Brisbane',
			state: 'QLD'
			}
		}
	}
]

let headers = ['Name', 'Email', 'State'];

//object.onclick = function(){ShowUsers};
btnGet.addEventListener('click', () => {
	let table = document.createElement('table');
	let headerRow = document.createElement('tr');

	headers.forEach(headerText =>{
		let header = document.createElement('th');
		let textNode = document.createTextNode(headerText);
		header.appendChild(textNode);
		headerRow.appendChild(header);
	});

	table.appendChild(headerRow);

	users.forEach(user => { 
		let row = document.createElement('tr');

		Object.values(user).forEach(text => {
			let cell = document.createElement('td');
			let textNode = document.createTextNode(text);
			cell.appendChild(textNode);
			row.appendChild(cell);
		})

		table.appendChild(row);
	});

	myTable.appentChild(table);
});
