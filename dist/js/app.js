(function(){function r(e,n,t){function o(i,f){if(!n[i]){if(!e[i]){var c="function"==typeof require&&require;if(!f&&c)return c(i,!0);if(u)return u(i,!0);var a=new Error("Cannot find module '"+i+"'");throw a.code="MODULE_NOT_FOUND",a}var p=n[i]={exports:{}};e[i][0].call(p.exports,function(r){var n=e[i][1][r];return o(n||r)},p,p.exports,r,e,n,t)}return n[i].exports}for(var u="function"==typeof require&&require,i=0;i<t.length;i++)o(t[i]);return o}return r})()({1:[function(require,module,exports){
'use strict';

// ## client.js ##
//
// This is the entrypoint for our application in the *web browser*.
// When the web page is loaded, this code will run in the client's web browser.

// Require React modules
var React = require('react');
var ReactDOM = require('react-dom');

// Require our root React component
var Root = require('./components/Root');

// Mount our React root component in the DOM
var mountPoint = document.getElementById('root');
ReactDOM.render(React.createElement(Root, null), mountPoint);

},{"./components/Root":2,"react":"react","react-dom":"react-dom"}],2:[function(require,module,exports){
'use strict';

// ## Root.js ##
//
// This is our top-level React component which contains all of our other
// components in a tree-like hierarchy. This component is mounted into the
// DOM in "client.js".

var React = require('react');

// Require our TaskList React component from "TaskList.js"
var TaskList = require('./TaskList');

// Here is where we actually define the Root component. At the moment it just
// contains a single component, TaskList.
var Root = function Root() {
  var element = React.createElement(TaskList, { heading: 'My task list' });
  return element;
};

// Export the Root component
module.exports = Root;

},{"./TaskList":3,"react":"react"}],3:[function(require,module,exports){
'use strict';

// ## TaskList.js ##
//
// The TaskList component renders a view for a list of tasks.

var React = require('react');

// Define the TaskList component
var TaskList = function TaskList(props) {
  // At the moment the TaskList component is just a hard-coded HTML unordered
  // list. We will be changing this during the lab.
  var element = React.createElement(
    'div',
    null,
    React.createElement(
      'h1',
      null,
      props.heading
    ),
    React.createElement(
      'ul',
      null,
      React.createElement(
        'li',
        null,
        'Clean my bed'
      ),
      React.createElement(
        'li',
        null,
        'Finish my homework'
      ),
      React.createElement(
        'li',
        null,
        'Brush my teeth'
      )
    )
  );
  return element;
};

// Export the TaskList component
module.exports = TaskList;

},{"react":"react"}]},{},[1])
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uL2RlcHMvbm9kZV9tb2R1bGVzL2Jyb3dzZXItcGFjay9fcHJlbHVkZS5qcyIsInNyYy9jbGllbnQuanMiLCJzcmMvY29tcG9uZW50cy9Sb290LmpzIiwic3JjL2NvbXBvbmVudHMvVGFza0xpc3QuanMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7OztBQ0FBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0EsSUFBTSxRQUFRLFFBQVEsT0FBUixDQUFkO0FBQ0EsSUFBTSxXQUFXLFFBQVEsV0FBUixDQUFqQjs7QUFFQTtBQUNBLElBQU0sT0FBTyxRQUFRLG1CQUFSLENBQWI7O0FBRUE7QUFDQSxJQUFNLGFBQWEsU0FBUyxjQUFULENBQXdCLE1BQXhCLENBQW5CO0FBQ0EsU0FBUyxNQUFULENBQWdCLG9CQUFDLElBQUQsT0FBaEIsRUFBMEIsVUFBMUI7Ozs7O0FDZEE7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQSxJQUFNLFFBQVEsUUFBUSxPQUFSLENBQWQ7O0FBRUE7QUFDQSxJQUFNLFdBQVcsUUFBUSxZQUFSLENBQWpCOztBQUVBO0FBQ0E7QUFDQSxJQUFNLE9BQU8sU0FBUCxJQUFPLEdBQU07QUFDakIsTUFBTSxVQUFVLG9CQUFDLFFBQUQsSUFBVSxTQUFRLGNBQWxCLEdBQWhCO0FBQ0EsU0FBTyxPQUFQO0FBQ0QsQ0FIRDs7QUFLQTtBQUNBLE9BQU8sT0FBUCxHQUFpQixJQUFqQjs7Ozs7QUNuQkE7QUFDQTtBQUNBOztBQUVBLElBQU0sUUFBUSxRQUFRLE9BQVIsQ0FBZDs7QUFFQTtBQUNBLElBQU0sV0FBVyxTQUFYLFFBQVcsQ0FBQyxLQUFELEVBQVc7QUFDMUI7QUFDQTtBQUNBLE1BQU0sVUFDSjtBQUFBO0FBQUE7QUFDRTtBQUFBO0FBQUE7QUFBTSxZQUFNO0FBQVosS0FERjtBQUVFO0FBQUE7QUFBQTtBQUNFO0FBQUE7QUFBQTtBQUFBO0FBQUEsT0FERjtBQUVFO0FBQUE7QUFBQTtBQUFBO0FBQUEsT0FGRjtBQUdFO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFIRjtBQUZGLEdBREY7QUFVQSxTQUFPLE9BQVA7QUFDRCxDQWREOztBQWdCQTtBQUNBLE9BQU8sT0FBUCxHQUFpQixRQUFqQiIsImZpbGUiOiJnZW5lcmF0ZWQuanMiLCJzb3VyY2VSb290IjoiIiwic291cmNlc0NvbnRlbnQiOlsiKGZ1bmN0aW9uKCl7ZnVuY3Rpb24gcihlLG4sdCl7ZnVuY3Rpb24gbyhpLGYpe2lmKCFuW2ldKXtpZighZVtpXSl7dmFyIGM9XCJmdW5jdGlvblwiPT10eXBlb2YgcmVxdWlyZSYmcmVxdWlyZTtpZighZiYmYylyZXR1cm4gYyhpLCEwKTtpZih1KXJldHVybiB1KGksITApO3ZhciBhPW5ldyBFcnJvcihcIkNhbm5vdCBmaW5kIG1vZHVsZSAnXCIraStcIidcIik7dGhyb3cgYS5jb2RlPVwiTU9EVUxFX05PVF9GT1VORFwiLGF9dmFyIHA9bltpXT17ZXhwb3J0czp7fX07ZVtpXVswXS5jYWxsKHAuZXhwb3J0cyxmdW5jdGlvbihyKXt2YXIgbj1lW2ldWzFdW3JdO3JldHVybiBvKG58fHIpfSxwLHAuZXhwb3J0cyxyLGUsbix0KX1yZXR1cm4gbltpXS5leHBvcnRzfWZvcih2YXIgdT1cImZ1bmN0aW9uXCI9PXR5cGVvZiByZXF1aXJlJiZyZXF1aXJlLGk9MDtpPHQubGVuZ3RoO2krKylvKHRbaV0pO3JldHVybiBvfXJldHVybiByfSkoKSIsIi8vICMjIGNsaWVudC5qcyAjI1xuLy9cbi8vIFRoaXMgaXMgdGhlIGVudHJ5cG9pbnQgZm9yIG91ciBhcHBsaWNhdGlvbiBpbiB0aGUgKndlYiBicm93c2VyKi5cbi8vIFdoZW4gdGhlIHdlYiBwYWdlIGlzIGxvYWRlZCwgdGhpcyBjb2RlIHdpbGwgcnVuIGluIHRoZSBjbGllbnQncyB3ZWIgYnJvd3Nlci5cblxuLy8gUmVxdWlyZSBSZWFjdCBtb2R1bGVzXG5jb25zdCBSZWFjdCA9IHJlcXVpcmUoJ3JlYWN0Jyk7XG5jb25zdCBSZWFjdERPTSA9IHJlcXVpcmUoJ3JlYWN0LWRvbScpO1xuXG4vLyBSZXF1aXJlIG91ciByb290IFJlYWN0IGNvbXBvbmVudFxuY29uc3QgUm9vdCA9IHJlcXVpcmUoJy4vY29tcG9uZW50cy9Sb290Jyk7XG5cbi8vIE1vdW50IG91ciBSZWFjdCByb290IGNvbXBvbmVudCBpbiB0aGUgRE9NXG5jb25zdCBtb3VudFBvaW50ID0gZG9jdW1lbnQuZ2V0RWxlbWVudEJ5SWQoJ3Jvb3QnKTtcblJlYWN0RE9NLnJlbmRlcig8Um9vdCAvPiwgbW91bnRQb2ludCk7XG4iLCIvLyAjIyBSb290LmpzICMjXG4vL1xuLy8gVGhpcyBpcyBvdXIgdG9wLWxldmVsIFJlYWN0IGNvbXBvbmVudCB3aGljaCBjb250YWlucyBhbGwgb2Ygb3VyIG90aGVyXG4vLyBjb21wb25lbnRzIGluIGEgdHJlZS1saWtlIGhpZXJhcmNoeS4gVGhpcyBjb21wb25lbnQgaXMgbW91bnRlZCBpbnRvIHRoZVxuLy8gRE9NIGluIFwiY2xpZW50LmpzXCIuXG5cbmNvbnN0IFJlYWN0ID0gcmVxdWlyZSgncmVhY3QnKTtcblxuLy8gUmVxdWlyZSBvdXIgVGFza0xpc3QgUmVhY3QgY29tcG9uZW50IGZyb20gXCJUYXNrTGlzdC5qc1wiXG5jb25zdCBUYXNrTGlzdCA9IHJlcXVpcmUoJy4vVGFza0xpc3QnKTtcblxuLy8gSGVyZSBpcyB3aGVyZSB3ZSBhY3R1YWxseSBkZWZpbmUgdGhlIFJvb3QgY29tcG9uZW50LiBBdCB0aGUgbW9tZW50IGl0IGp1c3Rcbi8vIGNvbnRhaW5zIGEgc2luZ2xlIGNvbXBvbmVudCwgVGFza0xpc3QuXG5jb25zdCBSb290ID0gKCkgPT4ge1xuICBjb25zdCBlbGVtZW50ID0gPFRhc2tMaXN0IGhlYWRpbmc9XCJNeSB0YXNrIGxpc3RcIiAvPjtcbiAgcmV0dXJuIGVsZW1lbnQ7XG59O1xuXG4vLyBFeHBvcnQgdGhlIFJvb3QgY29tcG9uZW50XG5tb2R1bGUuZXhwb3J0cyA9IFJvb3Q7XG4iLCIvLyAjIyBUYXNrTGlzdC5qcyAjI1xuLy9cbi8vIFRoZSBUYXNrTGlzdCBjb21wb25lbnQgcmVuZGVycyBhIHZpZXcgZm9yIGEgbGlzdCBvZiB0YXNrcy5cblxuY29uc3QgUmVhY3QgPSByZXF1aXJlKCdyZWFjdCcpO1xuXG4vLyBEZWZpbmUgdGhlIFRhc2tMaXN0IGNvbXBvbmVudFxuY29uc3QgVGFza0xpc3QgPSAocHJvcHMpID0+IHtcbiAgLy8gQXQgdGhlIG1vbWVudCB0aGUgVGFza0xpc3QgY29tcG9uZW50IGlzIGp1c3QgYSBoYXJkLWNvZGVkIEhUTUwgdW5vcmRlcmVkXG4gIC8vIGxpc3QuIFdlIHdpbGwgYmUgY2hhbmdpbmcgdGhpcyBkdXJpbmcgdGhlIGxhYi5cbiAgY29uc3QgZWxlbWVudCA9IChcbiAgICA8ZGl2PlxuICAgICAgPGgxPnsgcHJvcHMuaGVhZGluZyB9PC9oMT5cbiAgICAgIDx1bD5cbiAgICAgICAgPGxpPkNsZWFuIG15IGJlZDwvbGk+XG4gICAgICAgIDxsaT5GaW5pc2ggbXkgaG9tZXdvcms8L2xpPlxuICAgICAgICA8bGk+QnJ1c2ggbXkgdGVldGg8L2xpPlxuICAgICAgPC91bD5cbiAgICA8L2Rpdj5cbiAgKTtcbiAgcmV0dXJuIGVsZW1lbnQ7XG59O1xuXG4vLyBFeHBvcnQgdGhlIFRhc2tMaXN0IGNvbXBvbmVudFxubW9kdWxlLmV4cG9ydHMgPSBUYXNrTGlzdDtcbiJdfQ==
